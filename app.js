var server = require('./server');
var router = require("./router");
var handlers = require('./handlers');
var IP;
var handle = {}

handle["/"] = handlers.start;
handle["/start"] = handlers.start;
handle["/upload"] = handlers.upload;


handle["/mrostanski"] = handlers.mrostanski;

handle["/askraba"] = handlers.askraba; // handle Array

server.start(router.route, handle);
