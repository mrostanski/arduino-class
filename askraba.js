
function engine(response) {
    console.log("Request handler 'upload' was called.");
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.write("Hello Askraba functions here");
    response.end();
}

function aaa() {
    console.log("aaa invoked!");
}

exports.engine = engine; // exports show it to the file, that is using it (one level above)
exports.aaa = aaa;