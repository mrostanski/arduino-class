var http = require('http');
var url = require('url');
var exec = require("child_process").exec;
//var execSync = require('exec-sync');

//var IP = execSync("ip route get 8.8.8.8 | awk 'NR==1 {print $NF}'");


function start(route, handle) {
    http.createServer(function(req, res) {
        var pathname = url.parse(req.url).pathname;
        console.log("Request for " + pathname + " received.");
        route(handle, pathname, res);
    }).listen(process.env.PORT);
    console.log("Server started on " + process.env.IP + ", port " + process.env.PORT);
}

exports.start = start;
