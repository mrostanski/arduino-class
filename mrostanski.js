var fs = require("fs"); // spremenljivka za "file system", t.j. fs
var firmata = require("firmata"); // da so pini na Arduinu dostopni preko USB

function start(res) {
    console.log("Request handler 'upload' was called.");
    
    fs.readFile(__dirname + "/engine.html",
    function (err, data){
        if (err) {
            res.writeHead(500, {"Content-Type": "text/plain"});
            return res.end("Could not find engine.html");
        }
    res.writeHead(200);
    res.end(data);
    })
    
    var board = new firmata.Board("/dev/ttyACM0", function() {
        try { // try to open the next piece of code
            console.log("Starting Arduino and activating Pins 2 and 3");
            board.pinMode(2, board.MODES.OUTPUT); // pin za smer na H-mostu (H-bridge)
            board.pinMode(3, board.MODES.PWM); // hitrost Pulse Width Modulation
        }
        catch (err) { // if the error is generated, the response in the log is given
            console.log("Something went wrong with Arduino connection!");
            process.exit(-1); // exit the application completely (return code -1 (or 255))
        }
        console.log("Firmware: " + board.firmware.name + " - " + board.firmware.version.major);
        console.log("Connected to Arduino");
        
    });
    
}

exports.start = start; 