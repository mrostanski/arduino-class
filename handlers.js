var exec = require("child_process").exec;
var mrostanskiFunctions = require("./mrostanski");
var askrabaFunctions = require("./askraba");

function start(response) {
    console.log("Request handler 'start' was called.");
    exec("ip route get 8.8.8.8 | awk 'NR==1 {print $NF}'", function (error, stdout, stderr) {
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.write("Server is on");
    response.end();
    });
}
function upload(response) {
    console.log("Request handler 'upload' was called.");
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.write("Hello Upload");
    response.end();
}
function mrostanski(response) {
    console.log("Request handler 'mrostanski' was called.");
    mrostanskiFunctions.start(response);
}

function askraba(response) {
    console.log("Request handler 'askraba' was called.");
    askrabaFunctions.start(response);
    askrabaFunctions.aaa();
}



exports.start = start;
exports.upload = upload;
exports.mrostanski = mrostanski;
